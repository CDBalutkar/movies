//
//  NetworkManager.swift
//  Movies
//
//  Created by Chinmay Balutkar on 14/01/22.
//

import Alamofire

class NetworkManager: NSObject {
    
    static let sharedInstance = NetworkManager()
    //Use below request if alamofire is integrated
    func makeRequest<T:Codable>(_ urlRequest: URLRequest, completion: @escaping (Result<T, Error>) -> Void) {
        if Reachability.isConnectedToNetwork(), let url = urlRequest.url, let method = urlRequest.method {
            AF.request(url, method: method).responseData { response in
                switch(response.result) {
                case .success(let data):
                    do {
                        // Initialize JSON Decoder
                        let decoder = JSONDecoder()
                        
                        // print json for model correction
                        // print(try JSONSerialization.jsonObject(with: data, options: .mutableContainers))
                        
                        // Configure JSON Decoder
                        decoder.dateDecodingStrategy = .iso8601
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        
                        // Decode JSON Response
                        let genericData = try decoder.decode(T.self, from: data)
                        
                        // Invoke Handler
                        completion(.success(genericData))
                    }catch let error as DecodingError {
                        switch error {
                        case .typeMismatch(let key, let value):
                            print("error \(key), value \(value) and ERROR: \(error.localizedDescription)")
                        case .valueNotFound(let key, let value):
                            print("error \(key), value \(value) and ERROR: \(error.localizedDescription)")
                        case .keyNotFound(let key, let value):
                            print("error \(key), value \(value) and ERROR: \(error.localizedDescription)")
                        case .dataCorrupted(let key):
                            print("error \(key), and ERROR: \(error.localizedDescription)")
                        default:
                            print("ERROR: \(error.localizedDescription)")
                        }
                        completion(.failure(error))
                    } catch let error {
                        completion(.failure(error))
                    }
                    break
                case .failure(let error):
                    completion(.failure(error))
                    break
                }
            }
        } else {
            let errorTemp = NSError(domain:urlRequest.url?.absoluteString ?? "", code:401, userInfo: [NSLocalizedDescriptionKey: Constants.networkErrorMessage])
            completion(.failure(errorTemp))
        }
    }
    
    //Use below request if alamofire is not integrated for
    func makeURLSessionDataTaskRequest<T:Codable>(_ request: URLRequest, completion: @escaping (Result<T, Error>) -> Void){
        if Reachability.isConnectedToNetwork() {
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let data = data {
                    do {
                        // Initialize JSON Decoder
                        let decoder = JSONDecoder()
                        
                        // print json for model correction
                        // print(try JSONSerialization.jsonObject(with: data, options: .mutableContainers))
                        
                        // Configure JSON Decoder
                        decoder.keyDecodingStrategy = .convertFromSnakeCase
                        
                        // Decode JSON Response
                        let genericData = try decoder.decode(T.self, from: data)
                        
                        // Invoke Handler
                        completion(.success(genericData))
                    } catch let error as DecodingError {
                        switch error {
                        case .typeMismatch(let key, let value):
                            print("error \(key), value \(value) and ERROR: \(error.localizedDescription)")
                        case .valueNotFound(let key, let value):
                            print("error \(key), value \(value) and ERROR: \(error.localizedDescription)")
                        case .keyNotFound(let key, let value):
                            print("error \(key), value \(value) and ERROR: \(error.localizedDescription)")
                        case .dataCorrupted(let key):
                            print("error \(key), and ERROR: \(error.localizedDescription)")
                        default:
                            print("ERROR: \(error.localizedDescription)")
                        }
                        completion(.failure(error))
                    } catch let error {
                        completion(.failure(error))
                    }
                } else {
                    if let error = error {
                        print("Unable to Fetch  \(error)")
                        completion(.failure(error))
                    } else {
                        print("Unable to Fetch ")
                    }
                }
            }.resume()
        } else {
            let errorTemp = NSError(domain:request.url?.absoluteString ?? "", code:401, userInfo: [NSLocalizedDescriptionKey: Constants.networkErrorMessage])
            completion(.failure(errorTemp))
        }
    }
}

