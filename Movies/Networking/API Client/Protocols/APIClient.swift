import Foundation

internal protocol APIClient: AnyObject {
    
    var apiKey: String { get set }
    
    var baseUrl: URL { get set }
    
    var imageBaseURL: URL { get set }
    
    init(apiKey: String, baseUrl: URL, imageBaseURL: URL)

}
