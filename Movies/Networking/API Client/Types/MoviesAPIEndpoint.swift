import Foundation
import Alamofire

internal enum MoviesAPIEndpoint: Hashable {
    
    // MARK: - Cases
    
    case popularMovies
    case movieDetails(Int)
    
    // MARK: - Properties
    var path: String {
        switch self {
        case .popularMovies:
            return "popular"
        case .movieDetails(let id):
            return "\(id)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .popularMovies, .movieDetails:
            return .get
        }
    }
}
