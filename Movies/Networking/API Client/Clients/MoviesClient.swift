import Foundation

final class MoviesClient: APIClient {
    

    // MARK: - Properties

    internal var apiKey: String
    
    internal var baseUrl: URL
    
    internal var imageBaseURL: URL

    // MARK: - Initialization

    init(apiKey: String, baseUrl: URL, imageBaseURL: URL) {
        // Set Properties
        self.apiKey = apiKey
        self.baseUrl = baseUrl
        self.imageBaseURL = imageBaseURL
    }

    // MARK: - Public API

    func fetchPopularMovies(pageNumber: Int, _ completion: @escaping (Result<PopularMoviesData, Error>) -> Void) {
        // Create and Initiate Data Task
        let queryItem = URLQueryItem(name: Constants.pageText, value: String(pageNumber))
        let request = getRequest(for: .popularMovies, with: [queryItem])
        NetworkManager.sharedInstance.makeRequest(request, completion: completion)
    }
    
    func fetchMovieDetails(movieID: Int, _ completion: @escaping (Result<MovieDetailData, Error>) -> Void) {
        // Create and Initiate Data Task
        let request = getRequest(for: .movieDetails(movieID))
        NetworkManager.sharedInstance.makeRequest(request, completion: completion)
        // Call below only when any Json is failing
        // NetworkManager.sharedInstance.makeURLSessionDataTaskRequest(request, completion: completion)
    }

    // MARK: - Helper Methods
    private func getRequest(for endpoint: MoviesAPIEndpoint, with aditionalQueryItems: [URLQueryItem]? = nil) -> URLRequest {
        // Create URL
        let url = baseUrl.appendingPathComponent(endpoint.path)
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            return URLRequest(url: url)
        }
        //Query Parameters
        var queryItems: [URLQueryItem] = [URLQueryItem(name: Constants.apiKeyText, value: apiKey), URLQueryItem(name: Constants.languageText, value: Constants.enUS)]
        
        if let aditionalQueryItems = aditionalQueryItems {
            queryItems.append(contentsOf: aditionalQueryItems)
        }
        components.queryItems = queryItems
        
        // Create Request
        var request = URLRequest(url: components.url ?? url)
        request.httpMethod = endpoint.method.rawValue
        
        // Configure Request
        request.addValue(Constants.applicationJson, forHTTPHeaderField: Constants.contentType)
        return request
    }

}
