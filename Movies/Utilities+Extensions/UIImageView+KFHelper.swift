//
//  UIImageView+KFHelper.swift
//  Movies
//
//  Created by Chinmay Balutkar on 15/01/22.
//

import Kingfisher
import UIKit

extension UIImageView {
    
    func setImageWith(path:String) {
        if let movieClient = Environment.apiClient as? MoviesClient {
            let imageURL =  movieClient.imageBaseURL.appendingPathComponent(path)
            self.kf.indicatorType = .activity
            self.kf.setImage(with: imageURL)
        }
    }
    
}
