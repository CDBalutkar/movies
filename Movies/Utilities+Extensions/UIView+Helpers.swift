//
//  UIView+Helpers.swift
//  Movies
//
//  Created by Chinmay Balutkar on 14/01/22.
//

import UIKit

extension UIView {
    
    func setShadow() {
        self.clipsToBounds = false
        self.layer.shadowColor = UIColor.opaqueSeparator.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
    }
    
}
