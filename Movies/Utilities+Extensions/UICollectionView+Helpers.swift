import UIKit

extension UICollectionView {
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Unable to Dequeue Reusable Collection View Cell")
        }
        
        return cell
    }
    
    func registerFor<T: UICollectionViewCell>(cell: T.Type){
        let xib = UINib(nibName: cell.nibName, bundle: .main)
        register(xib, forCellWithReuseIdentifier: cell.reuseIdentifier)
    }
    
}
