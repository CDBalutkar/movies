//
//  UIViewController+Utils.swift
//  Movies
//
//  Created by Chinmay Balutkar on 16/01/22.
//

import UIKit

fileprivate var loaderView: UIView?

extension UIViewController {
    
    
    func showAlertWithMessage(_ message: String){
        let alert = UIAlertController(title: Constants.somethingWentWrongText, message:message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.okText, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showActivityIndicator() {
        DispatchQueue.main.async { [weak self] in
            if let strongSelf = self {
                let backgroundView = UIView(frame: strongSelf.view.bounds)
                backgroundView.backgroundColor = .white
                let activityIndicatorView = UIActivityIndicatorView(style: .medium)
                activityIndicatorView.center = backgroundView.center
                activityIndicatorView.startAnimating()
                backgroundView.addSubview(activityIndicatorView)
                loaderView = backgroundView
                strongSelf.view.addSubview(backgroundView)
            }
        }
    }
    
    func hideActivityIndicator() {
        DispatchQueue.main.async {
            loaderView?.removeFromSuperview()
            loaderView = nil
        }
    }
    
}
