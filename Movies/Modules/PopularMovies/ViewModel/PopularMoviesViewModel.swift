//
//  PopularMoviesViewModel.swift
//  Movies
//
//  Created by Chinmay Balutkar on 20/11/21.
//

import Foundation
import RxCocoa

class PopularMoviesViewModel {
    
    let segueIdentifier = Constants.showMovieDetailsSegueIdentifier
    let title = Constants.popularMovies
    var popularMoviesData: BehaviorRelay<PopularMoviesData?> = BehaviorRelay(value: nil)
    var error: BehaviorRelay<Error?> = BehaviorRelay(value: nil)
    private let apiClient:MoviesClient = Environment.apiClient as! MoviesClient
    private var selectedMovieIndex: Int?
    
    
    init() {
        // Fetch Popular Movies
        fetchPopularMovies()
    }
    
    
    func fetchPopularMovies(pageNumber: Int = 1) {
        if pageNumber != self.popularMoviesData.value?.pageNumber {
            apiClient.fetchPopularMovies(pageNumber: pageNumber) { [unowned self] (result) in
                switch result {
                case .success(var popularMoviesData):
                    if let oldPopularMovies = self.popularMoviesData.value {
                        popularMoviesData.updatePopularMoviesWith(oldPopularMovies.popularMovies)
                        self.popularMoviesData.accept(popularMoviesData)
                    } else {
                        self.popularMoviesData.accept(popularMoviesData)
                    }
                case .failure(let error):
                    self.error.accept(error)
                }
            }
        }
    }
    
    func setSelectedMovieIndex(_ index:Int) {
        selectedMovieIndex = index
    }
    
    func getSelectedMovieID() -> Int? {
        if let selectedMovieIndex = selectedMovieIndex, let popularMoviesData = self.popularMoviesData.value {
            return popularMoviesData.popularMovies[selectedMovieIndex].movieID
        } else {
            return nil
        }
    }
    
}
