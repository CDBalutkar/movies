//
//  MoviesCollectionViewCell.swift
//  Movies
//
//  Created by Chinmay Balutkar on 16/11/21.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieBannerImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }
     
    func setupView() {
        movieBannerImageView.contentMode = .scaleToFill
        containerView.setShadow()
    }
    
    

}
