//
//  NowPlayingMoviesViewController.swift
//  Movies
//
//  Created by Chinmay Balutkar on 16/11/21.
//

import UIKit
import RxSwift

class MoviesViewController: UIViewController, Storyboardable {
    
    var viewModel: PopularMoviesViewModel?
    private let disposeBag = DisposeBag()
    
    @IBOutlet var moviesCollectionView: UICollectionView! {
        didSet {
            moviesCollectionView.collectionViewLayout = createColectionViewLayout()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        moviesCollectionView.registerFor(cell: MoviesCollectionViewCell.self)
        self.showActivityIndicator()
        viewModel = PopularMoviesViewModel()
        self.title = viewModel?.title
        setupMoviesObserver()
    }
    
    private func createColectionViewLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(3/2))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(3/2))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [ item ])
        let section = NSCollectionLayoutSection(group: group)
        return UICollectionViewCompositionalLayout(section: section)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let viewModel = viewModel, segue.identifier == viewModel.segueIdentifier, let selectedMovieID = viewModel.getSelectedMovieID() {
            let movieDetailsViewController = segue.destination as! MovieDetailsViewController
            movieDetailsViewController.viewModel = MoviesDetailViewModel.init(movieID: selectedMovieID)
        }
    }
    
}

//MARK: RxSwift functions
extension MoviesViewController {
    
    func setupMoviesObserver() {
        if let viewModel = viewModel {
            
            viewModel.popularMoviesData.asObservable().subscribe(onNext: { [weak self] popularMoviesData in
                DispatchQueue.main.async {
                    if let strongSelf = self {
                        strongSelf.moviesCollectionView.reloadData()
                        strongSelf.hideActivityIndicator()
                    }
                }
            }).disposed(by: disposeBag)
            
            // If error occurs show
            viewModel.error.asObservable().subscribe(onNext: { [weak self] error in
                DispatchQueue.main.async {
                    // Show Alert with error object
                    if let strongSelf = self, let error = error {
                        strongSelf.showAlertWithMessage(error.localizedDescription)
                        strongSelf.hideActivityIndicator()
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
}

extension MoviesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let viewModel = viewModel, let popularMoviesDataValue =  viewModel.popularMoviesData.value {
            return popularMoviesDataValue.popularMovies.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MoviesCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        if let viewModel = viewModel, let popularMoviesDataValue =  viewModel.popularMoviesData.value {
            cell.movieBannerImageView.setImageWith(path: popularMoviesDataValue.popularMovies[indexPath.row].posterPath)
        }
        return cell
    }
    
    
}

extension MoviesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewModel = viewModel {
            viewModel.setSelectedMovieIndex(indexPath.row)
            performSegue(withIdentifier: viewModel.segueIdentifier, sender: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let viewModel = viewModel, let popularMoviesDataValue =  viewModel.popularMoviesData.value {
            if popularMoviesDataValue.pageNumber != popularMoviesDataValue.totalPages {
                if indexPath.row == popularMoviesDataValue.popularMovies.count-1 {
                    viewModel.fetchPopularMovies(pageNumber: popularMoviesDataValue.pageNumber + 1)
                }
            }
        }
    }
    
}




