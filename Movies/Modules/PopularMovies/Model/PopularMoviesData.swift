//
//  NowPlayingMoviesData.swift
//  Movies
//
//  Created by Chinmay Balutkar on 17/11/21.
//

import Foundation

// MARK: - Welcome
struct PopularMoviesData: Codable {
    var pageNumber: Int
    var popularMovies: [Movie]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case pageNumber = "page"
        case popularMovies = "results"
        case totalPages
        case totalResults
    }
    
    mutating func updatePopularMoviesWith(_ popularMovies:[Movie]) {
        self.popularMovies.insert(contentsOf: popularMovies, at: 0)
    }
}


// MARK: - Result
struct Movie: Codable {
    let isAdultMovie: Bool
    let backdropPath: String?
    let genreIds: [Int]
    let movieID: Int
    let originalTitle, overview: String
    let popularity: Double
    let posterPath, title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int
    let releaseDate: String?

    enum CodingKeys: String, CodingKey {
        case isAdultMovie = "adult"
        case backdropPath
        case genreIds
        case movieID = "id"
        case originalTitle
        case overview, popularity
        case posterPath
        case releaseDate
        case title, video
        case voteAverage
        case voteCount
    }
}

