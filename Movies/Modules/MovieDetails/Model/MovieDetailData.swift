//
//  MovieDetailData.swift
//  Movies
//
//  Created by Chinmay Balutkar on 17/11/21.
//

import Foundation

// MARK: - Welcome
struct MovieDetailData: Codable {
    let isAdultMovie: Bool
    let backdropPath, imdbID, originalLanguage, originalTitle : String
    let budget: Int
    let genres: [Genre]
    let homepage: String
    let movieID: Int
    let overview: String
    let popularity: Double
    let posterPath, releaseDate: String
    let productionCompanies: [ProductionCompany]
    let productionCountries: [ProductionCountry]
    let revenue, runtime: Int
    let spokenLanguages: [SpokenLanguage]
    let status, tagline, title: String
    let video: Bool
    let voteAverage: Float
    let voteCount: Int
    
    private let yearFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        return formatter
    }()
    
    private let durationFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [.hour, .minute]
        return formatter
    }()
    
    var ratingText: String {
        let rating = Int(voteAverage)
        let ratingText = (0..<rating).reduce("") { (acc, _) -> String in
            return acc + "★"
        }
        return ratingText
    }
    
    var scoreText: String {
        guard ratingText.count > 0 else {
            return "n/a"
        }
        return "\(ratingText.count)/10"
    }
    
    private var durationText: String {
        guard runtime > 0 else {
            return "n/a"
        }
        return durationFormatter.string(from: TimeInterval(runtime) * 60) ?? "n/a"
    }
    
    private var yearText: String {
        guard let date = String.dateFormatter.date(from: releaseDate) else {
            return "n/a"
        }
        return yearFormatter.string(from: date)
    }
    
    var movieYearDurationText: String {
            "\(yearText) · \(durationText)"
    }
    
    enum CodingKeys: String, CodingKey {
        case isAdultMovie = "adult"
        case movieID = "id"
        case backdropPath
        case genres
        case budget, homepage
        case imdbID = "imdbId"
        case originalLanguage
        case originalTitle
        case overview, popularity
        case posterPath
        case productionCompanies
        case productionCountries
        case releaseDate
        case revenue, runtime
        case spokenLanguages
        case status, tagline, title, video
        case voteAverage
        case voteCount
    }
    
    func getGenre() -> String {
        let genreNames = genres.compactMap() { genre in
            return genre.name
        }
        return genreNames.joined(separator: ", ")
    }
    
    func getProductionCompanyNames() -> String {
        let productionCompanyNames = productionCompanies.compactMap() { productionCompany in
            return productionCompany.name
        }
        return productionCompanyNames.joined(separator: ", ")
    }
    
    func getProductionCountryNames() -> String {
        let productionCountryNames = productionCountries.compactMap() { productionCountry in
            return productionCountry.name
        }
        return productionCountryNames.joined(separator: ", ")
    }
    
    func getSpokenLanguageNames() -> String {
        let spokenLanguageNames = spokenLanguages.compactMap() { spokenLanguage in
            return spokenLanguage.name
        }
        return spokenLanguageNames.joined(separator: ", ")
    }
}

// MARK: - Genre
struct Genre: Codable {
    let id: Int
    let name: String
}

// MARK: - ProductionCompany
struct ProductionCompany: Codable {
    let id: Int
    let logoPath: String?
    let name, originCountry: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case logoPath
        case name
        case originCountry
    }
}

// MARK: - ProductionCountry
struct ProductionCountry: Codable {
    let iso31661, name: String
    
    enum CodingKeys: String, CodingKey {
        case iso31661
        case name
    }
}

// MARK: - SpokenLanguage
struct SpokenLanguage: Codable {
    let englishName, iso6391, name: String
    
    enum CodingKeys: String, CodingKey {
        case englishName
        case iso6391
        case name
    }
}
