//
//  MoviesDetailViewModel.swift
//  Movies
//
//  Created by Chinmay Balutkar on 15/01/22.
//

import Foundation
import RxCocoa

class MoviesDetailViewModel {
    
    var movieDetailData: BehaviorRelay<MovieDetailData?> = BehaviorRelay(value: nil)
    var error: BehaviorRelay<Error?> = BehaviorRelay(value: nil)
    private let apiClient:MoviesClient = Environment.apiClient as! MoviesClient
    private var selectedMovieIndex: Int?
    
    init(movieID: Int) {
        // Fetch Popular Movies
        fetchMovieDataWith(movieID: movieID)
    }
    
    
    func fetchMovieDataWith(movieID: Int) {
        
        apiClient.fetchMovieDetails(movieID: movieID) { [weak self] (result) in
            if let strongSelf = self {
                switch result {
                case .success(let movieDetailData):
                    strongSelf.movieDetailData.accept(movieDetailData)
                case .failure(let error):
                    strongSelf.error.accept(error)
                }
            }
        }
    }
    
}
