//
//  MovieDetailsViewController.swift
//  Movies
//
//  Created by Chinmay Balutkar on 15/01/22.
//

import UIKit
import RxSwift

class MovieDetailsViewController: UIViewController {
    
    var viewModel: MoviesDetailViewModel?
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieBackdropImageView: UIImageView!
    @IBOutlet weak var movieOverviewLabel: UILabel!
    @IBOutlet weak var movieGenreLabel: UILabel!
    @IBOutlet weak var movieYearDurationLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var productionCompaniesLabel: UILabel!
    @IBOutlet weak var productionCountriesLabel: UILabel!
    @IBOutlet weak var spokenLanguageLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        showActivityIndicator()
        setupMoviesObserver()
        setupDefaultView()
    }
    
    private func setupDefaultView(){
        self.movieBackdropImageView.layer.cornerRadius = 5
        self.movieBackdropImageView.clipsToBounds = true
    }
    
    private func setupViewWith(_ movieDetailData: MovieDetailData){
        self.movieTitleLabel.text = movieDetailData.title
        self.movieOverviewLabel.text = movieDetailData.overview
        self.movieBackdropImageView.setImageWith(path: movieDetailData.backdropPath)
        self.movieGenreLabel.text = movieDetailData.getGenre()
        self.movieYearDurationLabel.text = movieDetailData.movieYearDurationText
        self.ratingLabel.text = movieDetailData.ratingText
        self.scoreLabel.text = movieDetailData.scoreText
        self.productionCountriesLabel.text = movieDetailData.getProductionCountryNames()
        self.productionCompaniesLabel.text = movieDetailData.getProductionCompanyNames()
        self.spokenLanguageLabel.text = movieDetailData.getSpokenLanguageNames()
    }

}

//MARK: RxSwift functions
extension MovieDetailsViewController {
    
    func setupMoviesObserver() {
        if let viewModel = viewModel {
            
            viewModel.movieDetailData.asObservable().subscribe(onNext: { [weak self] movieDetailData in
                DispatchQueue.main.async {
                    if let strongSelf = self, let movieDetailData = movieDetailData {
                        strongSelf.setupViewWith(movieDetailData)
                        strongSelf.hideActivityIndicator()
                    }
                }
            }).disposed(by: disposeBag)
            
            // If error occurs show
            viewModel.error.asObservable().subscribe(onNext: { [weak self] error in
                DispatchQueue.main.async {
                    if let strongSelf = self, let error = error {
                        strongSelf.showAlertWithMessage(error.localizedDescription)
                        strongSelf.hideActivityIndicator()
                    }
                }
            }).disposed(by: disposeBag)
        }
    }
    
}
