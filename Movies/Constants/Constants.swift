//
//  Constants.swift
//  Movies
//
//  Created by Chinmay Balutkar on 16/01/22.
//

import Foundation

public struct Constants {
    
    static let networkErrorMessage = "Internet Or Wifi not Available !! Please connect to internet via mobile network or WIFI and try again"
    static let apiKeyText = "api_key"
    static let languageText = "language"
    static let enUS = "en-US"
    static let contentType = "Content-Type"
    static let applicationJson = "application/json"
    static let pageText = "page"
    static let showMovieDetailsSegueIdentifier = "showMovieDetails"
    static let popularMovies = "Popular Movies"
    static let somethingWentWrongText = "Opps !! Something Went Wrong !!"
    static let okText = "Ok"
    
}
