//
//  Environment.swift
//  Cocoacasts
//
//  Created by Bart Jacobs on 29/03/2019.
//  Copyright © 2019 Code Foundry. All rights reserved.
//

import Foundation

enum Environment: String {
    
    // MARK: - Environments
    
    case debug
    case release

    // MARK: - Current Environment
    
    static let current: Environment = {
        // Read Value From Info.plist
        guard let value = Bundle.main.infoDictionary?["CONFIGURATION"] as? String else {
            fatalError("No Configuration Found")
        }
        
        // Create Environment
        guard let environment = Environment(rawValue: value.lowercased()) else {
            fatalError("Invalid Environment")
        }
        
        return environment
    }()

    // MARK: - Base URL
    
    private static var imageBaseUrl: URL {
        switch current {
        case .debug, .release:
            //https://image.tmdb.org/t/p/original/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg // original image
            //https://image.tmdb.org/t/p/w500/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg // less size image
            return URL(string: "https://image.tmdb.org/t/p/w500")!
        }
    }
    
    private static var baseUrl: URL {
        switch current {
        case .debug, .release:
            return URL(string: "https://api.themoviedb.org/3/movie/")!
        }
    }
    
    // MARK: - API Key
    
    private static var apiKey: String {
        switch current {
        case .debug, .release:
            return "a07e22bc18f5cb106bfe4cc1f83ad8ed"
        }
    }
    
    // MARK: - API Client

    static var apiClient: APIClient {
        switch current {
            case .debug, .release:
                return MoviesClient(apiKey: apiKey, baseUrl: baseUrl, imageBaseURL: imageBaseUrl)
        }
    }
    
}
